import PropTypes from 'prop-types';

/**
 * React Router match object
 * @see https://reacttraining.com/react-router/web/api/match
 *
 * @property {bool} isExact
 * @property {Object} params
 * @property {String} path
 * @property {String} url
 */
const REACT_ROUTER_MATCH_PROP_TYPE = PropTypes.shape({
	isExact: PropTypes.bool,
	params: PropTypes.object,
	path: PropTypes.string,
	url: PropTypes.string,
});

export default REACT_ROUTER_MATCH_PROP_TYPE;
