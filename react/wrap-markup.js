/**
 * Takes a string and wraps it in an object to be consumed by React's [dangerouslySetInnerHTML](https://reactjs.org/docs/dom-elements.html#dangerouslysetinnerhtml)
 *
 * @param {String} html
 * @return {Object}
 *   @property {String} __html
 */
export default function wrapMarkup(html) {
	return {
		__html: html,
	};
}
