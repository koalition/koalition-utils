# `koalition-utils`: React

This folder contains helper functions related to working with React. There might be some overlap with the `/wordpress` and `/redux` folders, but this folder is meant for helper functions that doesn't fall into either of those categories.
