# `koalition-utils`: Router

This folder contains a redux reducer (`./index.js`), and React Components for consuming state delivered by that reducer.
