import pathToRegexp from 'path-to-regexp';

/**
 * Match given pattern to given path
 * @param {String} pattern
 * @param {String} path
 * @return {Object}
 *   @property result
 *   @property keys
 */
export default function matchPath(pattern, path) {
	const result = {
		path,
		pattern,
		params: {},
	};

	// Return result immediately if wildcard pattern
	if (pattern === '*') return result;

	// Check pattern against path and map any detected params
	const keys = [];
	const regexp = pathToRegexp(pattern, keys);
	const match = path.match(regexp);
	if (match) {
		const values = match.slice(1);
		const params = {};
		keys.forEach(({ name }, index) => (params[name] = values[index]));
		return {
			...result,
			params,
		};
	}

	// No match, return null
	return null;
}
