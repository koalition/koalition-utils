import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import matchPath from '../match-path';
import { replace } from '../router.js';

/**
 * @class
 * Redirects to a given path if current path matches
 */
class Redirect extends React.Component {
	// -------------------- Static and instance settings / initialization --------------------

	/**
	 * @type {Object}
	 * @property {Function} dispatch - Passed in via `react-redux` and  `connect`
	 * @property {String} from
	 * @property {String} to
	 * @property {Object} router - Passed in via redux
	 */
	static propTypes = {
		dispatch: PropTypes.func.isRequired,
		from: PropTypes.string.isRequired,
		to: PropTypes.string.isRequired,
		router: PropTypes.object.isRequired,
	};

	// -------------------- React lifecycle --------------------

	componentDidMount() {
		this.testRedirect();
	}

	componentDidUpdate() {
		this.testRedirect();
	}

	render() {
		return null;
	}

	// -------------------- Helper methods --------------------

	testRedirect() {
		const { from: path, router } = this.props;

		if (path === '*') return this.redirect();

		const match = matchPath(path, router.pathname);
		if (match) return this.redirect();
	}

	redirect() {
		const { dispatch, to } = this.props;
		dispatch(replace(to));
	}
}

export default connect(({ router }) => ({ router }))(Redirect);
