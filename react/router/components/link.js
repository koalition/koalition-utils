import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import matchPath from '../match-path';
import { push } from '../router.js';

/**
 * @class
 * Link React component based on React Router's API.
 * Takes three main props: `children`, `component` and `to`.
 */
class Link extends React.Component {
	// -------------------- Static and instance settings / initialization --------------------

	/**
	 * @type {Object}
	 * @property children
	 * @property {String} component
	 * @property {Function} dispatch - Passed in via `react-redux` and  `connect`
	 * @property {String} to
	 */
	static propTypes = {
		activeClassName: PropTypes.string,
		children: PropTypes.any,
		className: PropTypes.string,
		component: PropTypes.any.isRequired,
		dispatch: PropTypes.func.isRequired,
		router: PropTypes.object.isRequired,
		to: PropTypes.string.isRequired,
	};

	static defaultProps = {
		component: 'a',
	};

	// -------------------- React lifecycle --------------------

	render() {
		const { children, component, to, ...rest } = this.props;

		delete rest.activeClassName;
		delete rest.className;
		delete rest.dispatch;
		delete rest.router;

		const href = component === 'a' ? to : null;
		return React.createElement(
			component,
			{
				href,
				...rest,
				className: this.getClassNames(),
				onClick: this.onClick,
			},
			children
		);
	}

	// -------------------- Helper methods --------------------

	onClick = event => {
		if (event && event.preventDefault) event.preventDefault();
		const { dispatch, to } = this.props;
		dispatch(push(to));
	};

	getClassNames() {
		const { activeClassName, className, to, router } = this.props;
		let cssClassName = className || '';
		if (activeClassName) {
			const match = matchPath(to, router.pathname);
			if (match) cssClassName += ` ${activeClassName}`;
		}
		return cssClassName;
	}
}

export default connect(({ router }) => ({ router }))(Link);
