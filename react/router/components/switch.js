import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import matchPath from '../match-path';

/**
 * @class
 * Renders first given child that matches the current path.
 * To work as intended all children must be `Route`s.
 */
class Switch extends React.Component {
	// -------------------- Static and instance settings / initialization --------------------

	/**
	 * @type {Object}
	 * @property children
	 * @property {Object} router - Passed in via redux
	 */
	static propTypes = {
		children: PropTypes.any.isRequired,
		router: PropTypes.object.isRequired,
	};

	// -------------------- React lifecycle --------------------

	render() {
		const { router } = this.props;
		let element, match;

		React.Children.forEach(this.props.children, child => {
			if (match == null && React.isValidElement(child)) {
				element = child;

				const path = child.props.path || child.props.from;

				match = matchPath(path, router.pathname);
			}
		});

		if (match) return React.cloneElement(element, { match });
		return null;
	}
}

export default connect(({ router }, { router: propsRouter }) => ({
	// This allows users to pass the router object themselves,
	// enabling transitions and other fun stuff
	router: propsRouter || router,
}))(Switch);
