import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import matchPath from '../match-path';

/**
 * @class
 * Renders children if current path matches given path.
 */
class Route extends React.Component {
	// -------------------- Static and instance settings / initialization --------------------

	/**
	 * @type {Object}
	 * @property children
	 * @property {String} path
	 * @property {Object} router - Passed in via redux
	 */
	static propTypes = {
		children: PropTypes.any.isRequired,
		match: PropTypes.object,
		path: PropTypes.string.isRequired,
		router: PropTypes.object.isRequired,
	};

	// -------------------- React lifecycle --------------------

	render() {
		const { children, path, router } = this.props;
		const match = this.props.match || matchPath(path, router.pathname);

		if (!match) return null;

		if (typeof children === 'function')
			return children({ match, path, router });
		return children;
	}
}

export default connect(({ router }) => ({ router }))(Route);
