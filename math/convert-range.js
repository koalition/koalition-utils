/**
 * Convert number from old range to new range
 * @see http://stackoverflow.com/questions/929103/convert-a-number-range-to-another-range-maintaining-ratio
 *
 * @param {Number} value - number to convert
 * @param {Number} oldMin - Start of range 1
 * @param {Number} oldMax - End of range 1
 * @param {Number} newMin - Start of range 2
 * @param {Number} newMax - End of range 2
 * @return {Number}
 */
const convertRange = (value, oldMin, oldMax, newMin = 0, newMax = 1) =>
	((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin) + newMin;
export default convertRange;
