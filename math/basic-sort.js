/**
 * Basic sorting function
 * @see https://stackoverflow.com/a/6712080/799327
 *
 * @param {Number}  a
 * @param {Number}  b
 * @return {Number} - Returns `-1`, `1` or `0`
 */
const basicSort = (a, b) => (a < b ? -1 : a > b ? 1 : 0);
export default basicSort;
