/**
 * Get the size an component with `aspect` needs to be to fully cover an area of `width` x `height`
 * @param {Number} aspect - Aspect ratio of covering asset
 * @param {Number} naturalWidth - Width of element we need to cover
 * @param {Number} naturalHeight - Height of element we need to cover
 * @return {Object}
 *   @property {Number} height
 *   @property {Number} width
 */
export default function getCoverSize(aspect, naturalWidth, naturalHeight) {
	let width = naturalWidth,
		height = naturalHeight;

	if (naturalWidth / naturalHeight > aspect) {
		height = width / aspect;
	} else {
		width = height * aspect;
	}

	height = Math.ceil(height);
	width = Math.ceil(width);

	return { height, width };
}
