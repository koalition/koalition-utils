/**
 * Normalized random
 * @see https://codepen.io/bionicoz/pen/xCIDH
 *
 * @param {Number} mean
 * @param {Number} standardDeviation
 */
const normalizedRandom = (mean, standardDeviation) =>
	Math.abs(
		Math.round(
			Math.random() * 2 - 1 + (Math.random() * 2 - 1) + (Math.random() * 2 - 1)
		) * standardDeviation
	) + mean;

export default normalizedRandom;
