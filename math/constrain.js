/**
 * Processing-like constrain utility.
 * Will constrain an input value to be between a given minimum and maximum value.
 *
 * @param {Number}  input
 * @param {Number}  min
 * @param {Number}  max
 * @return {Number}
 */
const constrain = (input, min, max) =>
	input < min ? min : input > max ? max : input;
export default constrain;
