import leftPad from 'left-pad';

/**
 * Formats a number of seconds into HH:MM:SS
 * @param  {Number} seconds
 * @param  {String} seperator
 * @return {String}
 */
export default function formatDuration(seconds, seperator = ':') {
	// Hours, minutes and seconds
	const hrs = ~~(seconds / 3600);
	const mins = ~~((seconds % 3600) / 60);
	const secs = Math.round(seconds % 60);

	// Output like '1:01' or '4:03:59' or '123:03:59'
	let output = '';

	if (hrs > 0) {
		output += hrs + seperator;
	}

	output += '' + leftPad(mins, 2, 0) + seperator + leftPad(secs, 2, 0);
	return output;
}
