/**
 * Calculate perceived brightness
 * @src http://alienryderflex.com/hsp.html
 * @param {number} r – number between 0 and 255
 * @param {number} g – number between 0 and 255
 * @param {number} b – number between 0 and 255
 * @return {number} - HSP value, should be between 0 and 255
 */
const hsp = (r, g, b) =>
	Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b));
export default hsp;
