import hsp from './hsp';

const BRIGHTNESS_THRESHOLD = 255 / 2;

/**
 * Using the HSP value, determine whether the color is light or dark
 * @src https://awik.io/determine-color-bright-dark-using-javascript/;
 * @param {object} color
 *   @prop {number} r - Between 0 and 255
 *   @prop {number} g - Between 0 and 255
 *   @prop {number} b - Between 0 and 255
 * @param {number} threshold - Defaults to 127.5 (50%)
 * @return {bool} - Returns true if the color is brighter than the given threshold
 */
const isColorLight = (
	{ r, g, b } = { r: 127, g: 128, b: 127 },
	threshold = BRIGHTNESS_THRESHOLD
) => hsp(r, g, b) > threshold;
export default isColorLight;
