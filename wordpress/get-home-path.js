/**
 * @overview
 * Get WP home path when using WPML with query string language parameter
 */

/**
 * Dependent on current and default languages being available in global object
 */
const { current: currentLang, default: defaultLang } = global.site.lang;

/**
 * Get root / home path with current language
 * @return {String}
 */
const getHomePath = () =>
	currentLang === defaultLang ? '/' : '/?lang=' + currentLang;
export default getHomePath;
