import log from '../log';
import { getPixelRatio } from '../browser/pixel-ratio';

/**
 * @overview
 * WP image object size selector helpers
 */

/**
 * Default image size
 */
let DEFAULT_SIZE_NAME = 'default_';

/**
 * Select default image from WP image object based on device width
 * @public
 *
 * @param {Object} imageObject - WP image object
 * @param {String} prefix - WP image family name, usually something like 'cinema_'
 * @param {Number}  minWidth - Minimum target width
 * @param {Number}  minHeight - Minimum target height
 * @param {Number}  pixelRatio - Device pixel ratio
 * @return {String} url
 */
export default function selectImageSize(
	imageObject,
	prefix = DEFAULT_SIZE_NAME,
	minWidth = window ? window.innerWidth : 0,
	minHeight = 0,
	pixelRatio = getPixelRatio()
) {
	if (typeof imageObject !== 'object' || typeof imageObject.url !== 'string') {
		log.warn(
			"selectImageSize() - Required parameter imageObject wasn't a valid WP image object.",
			imageObject
		);
		return null;
	}

	const { sizes, subtype } = imageObject;
	if (typeof sizes !== 'object') {
		log.warn(
			'selectImageSize() - imageObject had no size prop, returning original image URL.',
			imageObject
		);
		return imageObject.url;
	}

	if (subtype === 'svg+xml') {
		// log.warn('selectImageSize() - imageObject indicates that source image is vector graphics (SVG), return source URL.', imageObject);
		return imageObject.url;
	}

	minWidth *= pixelRatio;
	minHeight *= pixelRatio;

	let heightCandidate = 'h',
		widthCandidate = 'w',
		importantEdge = minWidth > minHeight ? 'w' : 'h', // This only decides what edge to deem as most important if no image fits the requirements.
		maxSize = -1,
		maxSizeLabel;

	// Loop thru all the sizes in the image object
	for (let label in sizes) {
		// Only test sizes that are part of our provided prefix
		if (label.indexOf(prefix) === 0) {
			// Compare width
			if (label.substr(label.length - 6) === '-width') {
				let imageLabel = label.substr(0, label.length - 6);

				// Figure out if an image is wide enough for our requirements
				if (sizes[label] > minWidth) {
					widthCandidate = imageLabel;
				}

				// Save this label, if it's the largest edge we've encountered so far
				if (importantEdge === 'w' && sizes[label] > maxSize) {
					maxSize = sizes[label];
					maxSizeLabel = imageLabel;
				}

				// Compare height
			} else if (label.substr(label.length - 7) === '-height') {
				let imageLabel = label.substr(0, label.length - 7);

				// Figure out if an image is high enough for our requirements
				if (sizes[label] > minHeight) {
					heightCandidate = imageLabel;
				}

				// Save this label, if it's the largest edge we've encountered so far
				if (importantEdge === 'h' && sizes[label] > maxSize) {
					maxSize = sizes[label];
					maxSizeLabel = imageLabel;
				}
			}

			// If an image is both wide and high enough, bail out and return it immediately
			if (heightCandidate === widthCandidate && sizes[heightCandidate]) {
				return sizes[heightCandidate];
			}
		}
	}

	// Apparently we found no images that's wide or high enough, so let's return the best one we got.
	if (!maxSizeLabel || !sizes[maxSizeLabel]) {
		log.warn(
			'selectImageSize() - No sizes in the given prefix range was found, returning original image URL.',
			prefix,
			imageObject
		);
		return imageObject.url;
	}

	return sizes[maxSizeLabel];
}
