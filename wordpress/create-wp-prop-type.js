import PropTypes from 'prop-types';

const createWpPropType = (propType, isRequired = false) =>
	isRequired
		? propType
		: PropTypes.oneOfType([propType, PropTypes.oneOf(['', false])]);

export default createWpPropType;
