import log from '../log';

/**
 * @overview
 * WP image object size selector helpers
 */

/**
 * Default image size
 */
let DEFAULT_SIZE_NAME = 'default_';

/**
 * Create srcSet and src attribute from WP image object
 * @public
 *
 * @param {Object} imageObject - WP image object
 * @param {String} prefix - WP image family name, usually something like 'cinema_'
 * @return {String} url
 */
export default function selectImageSrcSet(
	imageObject,
	prefix = DEFAULT_SIZE_NAME
) {
	if (typeof imageObject !== 'object' || typeof imageObject.url !== 'string') {
		log.warn(
			"selectImageSize() - Required parameter imageObject wasn't a valid WP image object.",
			imageObject
		);
		return null;
	}

	const { sizes, subtype } = imageObject;
	if (typeof sizes !== 'object') {
		log.warn(
			'selectImageSize() - imageObject had no size prop, returning original image URL.',
			imageObject
		);
		return { src: imageObject.url };
	}

	if (subtype === 'svg+xml') {
		// log.warn('selectImageSize() - imageObject indicates that source image is vector graphics (SVG), return source URL.', imageObject);
		return { src: imageObject.url };
	}

	let srcSet = '',
		srcSetObject = {},
		minWidth = Infinity,
		minWidthUrl;

	// Loop thru all the sizes in the image object
	for (let label in sizes) {
		// Only test sizes that are part of our provided prefix
		if (label.indexOf(prefix) === 0) {
			// Get width
			if (label.substr(label.length - 6) === '-width') {
				let url = sizes[label.substr(0, label.length - 6)];
				let width = sizes[label];
				srcSetObject[width] = url;

				if (width < minWidth) {
					minWidth = width;
					minWidthUrl = url;
				}
			}
		}
	}

	for (let width in srcSetObject) {
		srcSet += `${srcSetObject[width]} ${width}w,`;
	}

	// Remove trailing comma
	if (srcSet.substr(srcSet.length - 1, 1) === ',')
		srcSet = srcSet.substr(0, srcSet.length - 1);

	return { srcSet: srcSet || null, src: minWidthUrl || imageObject.url };
}
