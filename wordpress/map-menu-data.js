/**
 * Parse menu data from WP to be consistent with the rest of the API and easier to consume from React.
 *
 * @param {Object} item
 *   @property {Array} children
 *   @property {String} classes
 *   @property {String} ID
 *   @property {Number} order
 *   @property {String} parent
 *   @property {String} target
 *   @property {String} title
 *   @property {String} url
 * @return {Object}
 *   @property {Array} children
 *   @property {String} className
 *   @property {String} id
 *   @property {Number} order
 *   @property {String} parent
 *   @property {String} target
 *   @property {String} title
 *   @property {String} url
 */
const mapMenuData = ({ classes: className, ID: id, ...rest }) => ({
	className,
	id,
	...rest,
});

export default mapMenuData;
