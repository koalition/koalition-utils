import PropTypes from 'prop-types';
import createWpPropType from './create-wp-prop-type';

/**
 * Standard WP API link responses
 *
 * @property {String} target
 * @property {String} title
 * @property {String} url
 */
const WP_LINK_PROP_TYPE = createWpPropType(
	PropTypes.shape({
		title: PropTypes.string,
		url: PropTypes.string,
		target: PropTypes.oneOf(['_blank', '_parent', '_self', '_top', '']),
	})
);

export default WP_LINK_PROP_TYPE;
