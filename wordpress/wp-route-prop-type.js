import PropTypes from 'prop-types';

/**
 * Koalition WP route format (defined in `_routes.php`)
 *
 * @property {Number} id
 * @property {String} path
 * @property {String} slug
 * @property {String} template
 * @property {String} title
 * @property {bool} isFrontPage
 * @property {bool} isBlogPage
 */
const WP_ROUTE_PROP_TYPE = PropTypes.shape({
	id: PropTypes.number,
	path: PropTypes.string,
	slug: PropTypes.string,
	template: PropTypes.string,
	title: PropTypes.string,
	isFrontPage: PropTypes.bool,
	isBlogPage: PropTypes.bool
});

export default WP_ROUTE_PROP_TYPE;
