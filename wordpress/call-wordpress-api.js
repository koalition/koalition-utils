import 'isomorphic-fetch';

import { addQueryParam } from '../browser/url-parsers';
import log from '../log';

/**
 * Call WP api with on the given endpoint with the given parameters
 * @param {String} endpoint
 * @param {Object} params - `key: value` pairs
 * @return {Promise}
 */
export default function callWordpressApi(
	endpoint,
	params,
	requestOptions = {}
) {
	if (params) {
		for (let key in params) {
			endpoint = addQueryParam(endpoint, key, params[key]);
		}
	}

	// Cache buster if we're in debug mode
	if (global.site.isDebug) {
		log.debug(
			'callWordpressApi(' +
				endpoint +
				') - Site is in debug mode, cachebuster activated.'
		);
		endpoint = addQueryParam(endpoint, 'cachebuster', Date.now());
		endpoint = addQueryParam(endpoint, 'refresh-cache', true);
	}

	return fetch(endpoint, {
		headers: {
			'X-WP-Nonce': global.site.apiNonce,
			...requestOptions,
		},
		credentials: 'same-origin',
	})
		.then(response => response.json().then(json => ({ json, response })))
		.then(({ json, response }) =>
			response.ok ? { json, response } : Promise.reject(json)
		);
}
