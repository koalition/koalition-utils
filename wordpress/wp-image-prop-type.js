import PropTypes from 'prop-types';
import createWpPropType from './create-wp-prop-type';

/**
 * Standard WP API image responses
 *
 * @property {String} alt
 * @property {String} author
 * @property {String} caption
 * @property {String} date
 * @property {String} description
 * @property {String} filename
 * @property {Number} height
 * @property {String} icon
 * @property {Number} ID
 * @property {Number} id
 * @property {String} mime_type
 * @property {String} modified
 * @property {String} name
 * @property {Object} sizes
 * @property {String} title
 * @property {String} type
 * @property {String} url
 * @property {Number} width
 */
const WP_IMAGE_PROP_TYPE = PropTypes.shape({
	alt: PropTypes.string,
	author: PropTypes.string,
	caption: PropTypes.string,
	date: PropTypes.string,
	description: PropTypes.string,
	filename: PropTypes.string,
	height: createWpPropType(PropTypes.number),
	icon: PropTypes.string,
	ID: PropTypes.number,
	id: PropTypes.number,
	mime_type: PropTypes.string,
	modified: PropTypes.string,
	name: PropTypes.string,
	sizes: PropTypes.object,
	title: PropTypes.string,
	type: PropTypes.string,
	url: PropTypes.string,
	width: createWpPropType(PropTypes.number),
});

export default WP_IMAGE_PROP_TYPE;
