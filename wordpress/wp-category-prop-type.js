import PropTypes from 'prop-types';

/**
 * Standard WP API category responses
 * @see https://developer.wordpress.org/rest-api/reference/categories/
 *
 * @property {Number} count
 * @property {String} description
 * @property {Number} id
 * @property {String} link
 * @property {Object} meta
 * @property {String} name
 * @property {Number} parent
 * @property {String} slug
 * @property {String} taxonomy
 */
const WP_CATEGORY_PROP_TYPE = PropTypes.shape({
	count: PropTypes.number,
	description: PropTypes.string,
	id: PropTypes.number.isRequired,
	link: PropTypes.string,
	meta: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
	name: PropTypes.string.isRequired,
	parent: PropTypes.number,
	slug: PropTypes.string.isRequired,
	taxonomy: PropTypes.oneOf([
		'category',
		'link_category',
		'nav_menu',
		'post_format',
		'post_tag',
	]).isRequired,
});

export default WP_CATEGORY_PROP_TYPE;
