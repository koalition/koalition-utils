import PropTypes from 'prop-types';

/**
 * Standard WP API menu responses
 *
 * @property {String} attr
 * @property {Array} children
 * @property {String} classes
 * @property {String} description
 * @property {Number} ID
 * @property {Number} order
 * @property {Number} parent
 * @property {String} target
 * @property {String} title
 * @property {String} url
 */
const WP_MENU_PROP_TYPE = PropTypes.arrayOf(
	PropTypes.shape({
		attr: PropTypes.string,
		children: PropTypes.array,
		classes: PropTypes.string,
		description: PropTypes.string,
		ID: PropTypes.number.isRequired,
		order: PropTypes.number.isRequired,
		parent: PropTypes.number,
		target: PropTypes.string,
		title: PropTypes.string.isRequired,
		url: PropTypes.string.isRequired,
	})
);

export default WP_MENU_PROP_TYPE;
