import PropTypes from 'prop-types';

/**
 * Standard WP API post/page responses
 * @see https://developer.wordpress.org/rest-api/reference/pages/
 * @see https://developer.wordpress.org/rest-api/reference/posts/
 *
 * @property {Object} acf
 * @property {Number} author
 * @property {Array} categories
 * @property {String} comment_status
 * @property {Object} content
 * @property {String} date
 * @property {String} date_gmt
 * @property {Object} excerpt
 * @property {Number} featured_media
 * @property {Object} guid
 * @property {Number} id
 * @property {String} link
 * @property {Number} menu_order
 * @property {Array} meta
 * @property {String} modified
 * @property {String} modified_gmt
 * @property {Number} parent
 * @property {String} ping_status
 * @property {String} slug
 * @property {String} status
 * @property {Array} tags
 * @property {String} template
 * @property {Object} title
 */
const WP_POST_PROP_TYPE = PropTypes.shape({
	acf: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
	author: PropTypes.number,
	categories: PropTypes.array,
	comment_status: PropTypes.oneOf(['open', 'closed']),
	content: PropTypes.shape({ rendered: PropTypes.string }),
	date: PropTypes.string,
	date_gmt: PropTypes.string,
	excerpt: PropTypes.shape({ rendered: PropTypes.string }),
	featured_media: PropTypes.number,
	guid: PropTypes.shape({ rendered: PropTypes.string }),
	id: PropTypes.number,
	link: PropTypes.string,
	menu_order: PropTypes.number,
	meta: PropTypes.array,
	modified: PropTypes.string,
	modified_gmt: PropTypes.string,
	parent: PropTypes.number,
	ping_status: PropTypes.oneOf(['open', 'closed']),
	slug: PropTypes.string,
	status: PropTypes.oneOf(['publish', 'future', 'draft', 'pending', 'private']),
	tags: PropTypes.array,
	template: PropTypes.string,
	title: PropTypes.shape({ rendered: PropTypes.string }),
});

export default WP_POST_PROP_TYPE;
