import toNumber from 'lodash.tonumber';
import isEmpty from 'lodash.isempty';
import log from '../log';

/**
 * Get a given value based on size.
 * This is useful for instance for selecting a specific video
 * for a player of a given width. We create an object of all
 * our videos - e.g. { '640': '{URL}', '1280': '{URL}' }, and
 * then pass this object and the current width of the player
 * to this function.
 *
 * @param {Object} values
 * @property {String} {Number} - set of sizes with corresponding values
 * @param {integer} targetSize
 * @return value
 */
export default function getValueFromSize(values, targetSize) {
	if (typeof values !== 'object' || isEmpty(values)) {
		log.debug(
			'getValueFromSize() - Required parameter "values" wasn\'t a valid object. ',
			values
		);
		return;
	}

	// get an array of the sizes
	const sizes = Object.keys(values)
		.filter(size => values[size])
		.map(size => toNumber(size));

	// set the largest as closest in case it is over
	let closest = Math.max.apply(null, sizes);

	sizes.forEach(size => {
		if (size >= targetSize && size < closest) closest = size;
	});

	return values[closest];
}
