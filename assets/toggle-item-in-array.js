/**
 * Add or remove a given item from a given array. Doesn't mutate the given arguments.
 *
 * @param item
 * @param array {Array}
 * @return {Array} - New array
 */
export default function toggleItemInArray(item, array) {
	const index = array.indexOf(item);

	// If the item isn't in the array, add it
	if (index === -1) return [...array, item];

	// Else remove it
	const newArray = [...array];
	newArray.splice(index, 1);
	return newArray;
}
