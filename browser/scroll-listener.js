import raf from 'raf';
import getScroll from './get-scroll';

const hasWheelEvent = 'onwheel' in document;
const hasMouseWheelEvent = 'onmousewheel' in document;
const hasTouch = 'ontouchstart' in document;
const hasTouchWin =
	navigator.msMaxTouchPoints && navigator.msMaxTouchPoints > 1;
const hasPointer = !!global.navigator.msPointerEnabled;
const hasKeyDown = 'onkeydown' in document;

const DEFAULTS = {
	fps: 20,
	timeout: 2000,
};

/**
 * @class
 * Virtual scroll listeners for DOM elements.
 * Based on [VirtualScroll](https://raw.githubusercontent.com/drojdjou/bartekdrozdz.com/master/static/src/framework/VirtualScroll.js).
 */
export default class ScrollListener {
	/**
	 * @constructor
	 * @param {Object} options
	 *   @property {Number} fps - frequency / updates per second
	 *   @property {Number} timeout - amount of time in ms to allow for same result before turning off listeners
	 */
	constructor(options) {
		this.options = Object.assign({}, DEFAULTS, options);
		this._listeners = [];
		this._internalListeners = [];
		this._lastSeen = getScroll();
	}

	/**
	 * Notify consumer listeners
	 * @private
	 *
	 * @param {Object} event
	 */
	_notify(event) {
		const { _listeners } = this;
		for (let i = 0; i < _listeners.length; i++) {
			_listeners[i](event);
		}
	}

	/**
	 * Continously listen for scroll position
	 * @private
	 *
	 * @param {Object} event - original event object
	 */
	_start(event) {
		this._stop();

		const { fps, timeout } = this.options;
		const { x, y } = getScroll();
		const { x: lastSeenX, y: lastSeenY } = this._lastSeen;
		const deltaX = x - (lastSeenX || x),
			deltaY = y - (lastSeenY || y);

		const internalEvent = {
			x: x,
			y: y,
			deltaX,
			deltaY,
			originalEvent: event,
		};
		this._notify(internalEvent);

		if (deltaX === 0 && deltaY === 0) {
			if (this._isStopping !== true) {
				this._isStopping = true;
				this._stopTimer = setTimeout(() => this._stop(), timeout);
			}
		} else {
			this._isStopping = false;
			clearTimeout(this._stopTimer);
		}

		this._lastSeen = {
			x,
			y,
		};

		if (fps) {
			this._startTimer = setTimeout(
				() =>
					(this._animationFrame = raf(() => {
						this._start(event);
					})),
				1000 / fps
			);
		} else {
			this._animationFrame = raf(() => {
				this._start(event);
			});
		}
	}

	/**
	 * Stop continous listener
	 * @private
	 */
	_stop() {
		clearTimeout(this._startTimer);
		raf.cancel(this._animationFrame);
	}

	/**
	 * Register internal listeners
	 * @private
	 */
	_initListeners() {
		this._destroyListeners();

		this._addListeners();

		this._internalListeners = [];

		this._isInitialized = true;
	}

	/**
	 * Add internal listeners to a single element
	 * @private
	 *
	 * @param {HTMLElement} element
	 */
	_addListeners(element = document) {
		let syntheticListener = event => this._start(event);
		element.addEventListener('syntheticScroll', syntheticListener);
		this._internalListeners.push({
			element: element,
			type: 'synthetic',
			listener: syntheticListener,
		});

		let scrollListener = event => this._start(event);
		element.addEventListener('scroll', scrollListener);
		this._internalListeners.push({
			element: element,
			type: 'scroll',
			listener: scrollListener,
		});

		if (hasWheelEvent) {
			let wheelListener = event => this._start(event);
			element.addEventListener('wheel', wheelListener);
			this._internalListeners.push({
				element: element,
				type: 'wheel',
				listener: wheelListener,
			});
		}

		if (hasMouseWheelEvent) {
			let mouseWheelListener = event => this._start(event);
			element.addEventListener('mousewheel', mouseWheelListener);
			this._internalListeners.push({
				element: element,
				type: 'mousewheel',
				listener: mouseWheelListener,
			});
		}

		if (hasTouch) {
			let touchStartListener = event => this._start(event);
			element.addEventListener('touchstart', touchStartListener);
			this._internalListeners.push({
				element: element,
				type: 'touchstart',
				listener: touchStartListener,
			});

			let touchMoveListener = event => this._start(event);
			element.addEventListener('touchmove', touchMoveListener);
			this._internalListeners.push({
				element: element,
				type: 'touchmove',
				listener: touchMoveListener,
			});
		}

		if (hasPointer && hasTouchWin) {
			this._bodyTouchAction = document.body.style.msTouchAction;
			document.body.style.msTouchAction = 'none';

			let touchStartListener = event => this._start(event);
			element.addEventListener('MSPointerDown', touchStartListener, true);
			this._internalListeners.push({
				element: element,
				type: 'MSPointerDown',
				listener: touchStartListener,
				useCapture: true,
			});

			let touchMoveListener = event => this._start(event);
			element.addEventListener('MSPointerMove', touchMoveListener, true);
			this._internalListeners.push({
				element: element,
				type: 'MSPointerMove',
				listener: touchMoveListener,
				useCapture: true,
			});
		}

		if (hasKeyDown) {
			let keyDownListener = event => this._start(event);
			element.addEventListener('keydown', keyDownListener);
			this._internalListeners.push({
				element: element,
				type: 'keydown',
				listener: keyDownListener,
			});
		}
	}

	/**
	 * Deregister internal listeners
	 * @private
	 */
	_destroyListeners() {
		this._internalListeners.forEach(obj =>
			obj.element.removeEventListener(obj.type, obj.listener, obj.useCapture)
		);

		this._isInitialized = false;
	}

	/**
	 * Hook for consumer to register VS listener
	 * @public
	 *
	 * @param {Function} listener
	 * @return {Number} index
	 */
	on(listener) {
		if (!this._isInitialized) {
			this._initListeners();
		}

		return this._listeners.push(listener);
	}

	/**
	 * Hook for consumer to deregister VS listener
	 * @public
	 *
	 * @param {Number} index
	 */
	off(index) {
		if (index) {
			this._listeners.splice(index, 1);
		} else {
			this._listeners = [];
		}

		if (this._listeners.length <= 0) {
			this._destroyListeners();
		}
	}

	/**
	 * Destroy all listeners
	 * @public
	 */
	destroy() {
		this.off();
	}
}
