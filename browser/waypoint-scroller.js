import {
	scrollToElement,
	scrollDistance,
	isAnimatingScroll,
} from './scroll-to';

import 'waypoints/lib/noframework.waypoints';
const Waypoint = global.Waypoint;

/**
 * @class
 * Helper Class that adds functionality for scrolling to items in a list and updating state when they are scrolled to
 * @author brmh
 */
export default class WaypointScroller {
	/**
	 * @constructor
	 * @param {HTMLElement} parentElement
	 * @param {Array} elements
	 * @param {Function} waypointCallback
	 * @param {Number}  waypointOffset
	 */
	constructor(
		parentElement = document,
		elements = [],
		waypointCallback,
		waypointOffset
	) {
		this._vars = {
			waypoints: [],
			waypointCallback,
			waypointOffset,
		};

		this._refs = {
			parentElement,
			elements,
		};

		this.bindWaypoint();
	}

	/**
	 * Unbinds waypoints
	 */
	unbind() {
		this.unbindWaypoint();
	}

	/**
	 * Scroll to element from index
	 * @param {Number}  index
	 */
	scrollToIndex(index) {
		scrollToElement(this._refs.elements[index]);
	}

	/**
	 * Scroll to element from index
	 * @param {Number}  index
	 * @param {Number}  offset
	 */
	scrollToIndexWithOffset(index, offset) {
		const element = this._refs.elements[index];
		const elementTop = element.getBoundingClientRect().top;

		scrollDistance(elementTop - offset);
	}

	/**
	 * Refresh waypoints
	 */
	refresh() {
		Waypoint.refreshAll();
	}

	/**
	 * Bind waypoints
	 */
	bindWaypoint() {
		const { elements } = this._refs;
		const { waypointOffset } = this._vars;

		let waypoints = [];

		this.unbindWaypoint();

		// Bind waypoints to every element that updates menu as user (or we) scroll
		elements.forEach(element => {
			waypoints.push(
				new Waypoint({
					element,
					offset: waypointOffset,
					handler: direction => {
						if (!isAnimatingScroll() && direction === 'down') {
							this._vars.waypointCallback(elements.indexOf(element));
						}
					},
				})
			);

			waypoints.push(
				new Waypoint({
					element,
					offset: function() {
						return (
							-this.element.clientHeight +
							(typeof waypointOffset === 'function'
								? waypointOffset()
								: waypointOffset)
						);
					},
					handler: direction => {
						if (!isAnimatingScroll() && direction === 'up') {
							this._vars.waypointCallback(elements.indexOf(element));
						}
					},
				})
			);
		});

		this._vars.waypoints = waypoints;
	}

	/**
	 * Unbind waypoints
	 */
	unbindWaypoint() {
		if (this._vars.waypoints) {
			this._vars.waypoints.forEach(waypoint => {
				waypoint.destroy();
				waypoint = null;
			});
			this._vars.waypoints = null;
			delete this._vars.waypoints;
		}
	}
}
