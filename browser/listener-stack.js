/**
 * @class
 * A class to encapsulate adding and removing listeners in a React Component
 */
export default class ListenerStack {
	/**
	 * @constructor
	 */
	constructor() {
		this._stack = [];
	}

	/**
	 * Add listener to the listener stack
	 * @public
	 *
	 * @param {Object} node                A dom node to bind the listner to
	 * @param {String} type                Type of listener to add
	 * @param {Function} handler           A function to call when listener fires
	 * @param {Object or Boolean} options  The options to pass to the listener
	 * @return {Number}                    Return the stack index of the new listener
	 */
	addListener(node, type, handler, options) {
		node.addEventListener(type, handler, options);
		const length = this._stack.push({ node, type, handler, options });
		return length - 1;
	}

	/**
	 * Remove all listeners from the stack
	 * @public
	 */
	removeAllListeners() {
		this._stack = this._stack.filter(listener => {
			const { node, type, handler, options } = listener;
			node.removeEventListener(type, handler, options);
			return false;
		});
	}

	/**
	 * Remove a specific listener by index
	 * @public
	 *
	 * @param  {Number} index The stack index of the listener to remove
	 */
	removeListener(index) {
		const { node, type, handler, options } = this._stack[index];
		node.removeEventListener(type, handler, options);
		this._stack = this._stack.splice(index, 1);
	}
}
