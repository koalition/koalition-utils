import Modernizr from 'helpers/modernizr';

/**
 * Translate Y value as 3D value if 3D transforms are available
 *
 * @param {String} value
 * @return {String}
 */
const getTranslateY = value =>
	Modernizr.csstransforms3d
		? 'translate3d(0, ' + value + ', 0)'
		: 'translateY(' + value + ')';
export default getTranslateY;
