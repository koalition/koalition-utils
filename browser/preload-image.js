/**
 * Preload image from URL
 *
 * @param {String} url - required
 * @return {Promise}
 */
const preloadImage = url =>
	new Promise(function(resolve, reject) {
		if (!url || typeof url !== 'string')
			reject('preloadImage() - Invalid parameter `url` provided.');

		const cleanup = () => {
			image.removeEventListener('error', onError);
			image.removeEventListener('error', onLoad);
		};

		const onError = event => {
			reject('preloadImage() - Invalid parameter `url` provided.', event);
			cleanup();
		};
		const onLoad = event => {
			resolve(image, event);
			cleanup();
		};

		const image = new Image();

		image.addEventListener('error', onError);
		image.addEventListener('load', onLoad);

		image.src = url;
	});

export default preloadImage;
