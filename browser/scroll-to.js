'use strict';

import './custom-event-polyfill';
import AnimatedSpring from '../animation/animated-spring';
import getScroll from './get-scroll';
import ScrollListener from './scroll-listener';

/**
 * Scroll animation helper
 * @private
 */
const spring = new AnimatedSpring({
	acceleration: 0.05,
	margin: 1,
});

spring.on(({ current }) => setScrollTop(current));

/**
 * Prevent scroll events while we're animating
 * @private
 */
let listener;
document.addEventListener('DOMContentLoaded', () => {
	listener = new ScrollListener(document);
	listener.on(
		event =>
			isAnimatingScroll() === true && event.originalEvent.preventDefault()
	);
});

/**
 * Get scrollTop cross browser
 * @public
 */
export const getScrollTop = () => getScroll().y;

/**
 * Set scrollTop cross browser
 * @private
 *
 * @param {integer} px
 */
function setScrollTop(px) {
	document.documentElement.scrollTop = document.body.scrollTop = px;

	// Throw a synthetic scroll event, which in turn can be caught by the ScrollListener class
	const event = new CustomEvent('syntheticScroll', {
		bubbles: true,
		cancelable: false,
		detail: null,
		type: 'syntheticScroll',
	});
	document.dispatchEvent(event);
}

/**
 * Animate scrollTop to a given pixel value
 * @public
 *
 * @param {integer} target
 */
export default function scrollTo(target) {
	spring.state = {
		current: getScrollTop(),
		target: getScrollTop(),
	};

	spring.target = target;
	spring.start();

	return target;
}

/**
 * Animate scrollTop a given distance
 * @public
 *
 * @param {integer} distance
 */
export const scrollDistance = distance => scrollTo(getScrollTop() + distance);

/**
 * Animate scrollTop to given element
 * @public
 *
 * @param {DOMElement} element
 */
export const scrollToElement = element =>
	scrollTo(getScrollTop() + element.getBoundingClientRect().top);

/**
 * Getter for checking animation status
 * @public
 */
export const isAnimatingScroll = () => !!spring._isAnimating;
