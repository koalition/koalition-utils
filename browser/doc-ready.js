/**
 * Document ready (IE9+)
 * @see http://youmightnotneedjquery.com/#ready
 *
 * @param {Function} fn
 */
export default function docReady(fn) {
	if (
		document.attachEvent
			? document.readyState === 'complete'
			: document.readyState !== 'loading'
	) {
		fn();
	} else {
		document.addEventListener('DOMContentLoaded', fn);
	}
}
