/**
 * Get transform prop
 * @see http://stackoverflow.com/questions/8889014/setting-vendor-prefixed-css-using-javascript
 */
export const TRANSFORM_PROP = (function() {
	const testEl = document.createElement('div');
	if (testEl.style.transform == null) {
		const vendors = ['Webkit', 'Moz', 'ms'];
		for (const vendor in vendors) {
			if (testEl.style[vendors[vendor] + 'Transform'] !== undefined) {
				return vendors[vendor] + 'Transform';
			}
		}
	}
	return 'transform';
})();

/**
 * Standard transform object
 *
 * @param {String} value
 * @return {Object}
 *  @property [TRANSFORM_PROP] {String} - value
 */
const getTransform = value => ({ [TRANSFORM_PROP]: value });
export default getTransform;
