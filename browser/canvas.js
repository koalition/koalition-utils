import { getWindowSize, bindResizeEvents, unBindResizeEvents } from './resize';

/**
 * @class
 * Full window size canvas element
 */
export default class Canvas {
	/**
	 * Create canvas element
	 * @static

	 * @param {HTMLElement} container
	 * @return {HTMLElement} canvas
	 */
	static createCanvas(container = document.body) {
		const canvas = document.createElement('canvas');
		container.appendChild(canvas);
		return canvas;
	}

	/**
	 * Handle internal error
	 * @static
	 *
	 * @param message {String}
	 */
	static error(message) {
		throw Error(message);
	}

	/**
	 * @contructor
	 *
	 * @param {Object} settings
	 * @property {HTMLElement} canvas
	 * @property {HTMLElement} container
	 * @property {bool} hasResize
	 * @property {Function} resizeListener
	 * @return {Object} this - class instance
	 */
	constructor({
		canvas = null,
		container = document.body,
		hasResize = true,
		resizeListener = null,
	} = {}) {
		canvas = canvas || Canvas.createCanvas(container);

		this._vars = {
			canvas,
		};

		this.settings = {
			hasResize,
			resizeListener,
		};

		this._setup();

		return this;
	}

	/**
	 * Set up resize listener
	 * @private
	 */
	_setup() {
		const { canvas } = this._vars;

		if (!canvas)
			return Canvas.error(
				'Canvas._setup() - Trying to set up canvas with no canvas available.'
			);

		// Add resize listener
		const { hasResize, resizeListener } = this.settings;
		if (hasResize || resizeListener) {
			const listener = event => this.resize(event);
			bindResizeEvents(listener);
			this._vars.resizeListener = listener;
		}

		// Set initial canvas size
		this.resize();
	}

	/**
	 * Resize canvas to fit to screen
	 * @public
	 *
	 * @param {Object} event
	 * @return {Object} size
	 */
	resize(event) {
		const { canvas } = this._vars;

		if (!canvas)
			return Canvas.error(
				'Canvas.resize() - Trying to resize canvas with no canvas available.'
			);

		const size = getWindowSize();
		canvas.width = size.innerWidth;
		canvas.height = size.innerHeight;

		const { resizeListener } = this.settings;
		if (resizeListener) resizeListener(event, this);

		return size;
	}

	/**
	 * Resize canvas to fit to screen
	 * @public
	 *
	 * @param {String} type - https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/getContext
	 * @param {Object} attributes - https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/getContext
	 * @return {RenderingContext} context
	 */
	getContext(type = '2d', attributes) {
		const { canvas } = this._vars;

		if (!canvas)
			return Canvas.error(
				'Canvas.getContext() - Trying to get context with no canvas available.'
			);

		return canvas.getContext(type, attributes);
	}

	/**
	 * Cleanup / destroy
	 * @public
	 */
	destroy() {
		const { canvas, resizeListener } = this._vars;

		// Remove canvas
		canvas.parentNode.removeChild(canvas);
		this._vars.canvas = null;

		// Unbind listeners
		if (resizeListener) {
			unBindResizeEvents(resizeListener);
			this._vars.resizeListener = null;
		}
	}

	/**
	 * Get canvas HTMLElement
	 * @public
	 *
	 * @return {HTMLElement} canvas
	 */
	get element() {
		const { canvas } = this._vars;

		if (!canvas)
			return Canvas.error(
				'Canvas.element - Trying to get canvas element with no canvas available.'
			);

		return canvas;
	}

	/**
	 * Get width of canvas HTMLElement
	 * @public
	 *
	 * @return {Number} width
	 */
	get width() {
		const { canvas } = this._vars;

		if (!canvas)
			return Canvas.error(
				'Canvas.width - Trying to get width with no canvas available.'
			);

		return canvas.width;
	}

	/**
	 * Get height of canvas HTMLElement
	 * @public
	 *
	 * @return {Number} width
	 */
	get height() {
		const { canvas } = this._vars;

		if (!canvas)
			return Canvas.error(
				'Canvas.height - Trying to get height with no canvas available.'
			);

		return canvas.height;
	}

	/**
	 * Set width of canvas HTMLElement
	 * @public
	 *
	 * @param {Number} value
	 * @return {Number} width
	 */
	set width(value) {
		const { canvas } = this._vars;

		if (!canvas)
			return Canvas.error(
				'Canvas.width - Trying to set width with no canvas available.'
			);

		return (canvas.width = value);
	}

	/**
	 * Set height of canvas HTMLElement
	 * @public
	 *
	 * @param {Number} value
	 * @return {Number} width
	 */
	set height(value) {
		const { canvas } = this._vars;

		if (!canvas)
			return Canvas.error(
				'Canvas.height - Trying to set height with no canvas available.'
			);

		return (canvas.height = value);
	}

	/**
	 * Set resizeListener
	 * @public
	 *
	 * @param {Function} value
	 */
	set resizeListener(value) {
		const { canvas } = this._vars;

		if (!canvas)
			return Canvas.error(
				'Canvas.height - Trying to set height with no canvas available.'
			);

		this._vars.resizeListener = value;
		this.resize();
	}
}
