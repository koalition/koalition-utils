/**
 * Get position of element from viewport top left
 * @see https://stackoverflow.com/a/24829409/799327
 *
 * @param {HTMLElement} element
 * @return {Object}
 *   @property {Number} x
 *   @property {Number} y
 */
export default function getPosition(element) {
	let x = 0;
	let y = 0;

	while (element) {
		x += element.offsetLeft - element.scrollLeft + element.clientLeft;
		y += element.offsetTop - element.scrollTop + element.clientTop;

		element = element.offsetParent;

		if (element === document.body) element = null;
	}

	return { x, y };
}
