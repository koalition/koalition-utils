/**
 * Load script from given URL
 *
 * @param {String} src
 * @param {HTMLElement} container
 * @return {Promise}
 **/
export default function loadScript(src, container = document.head) {
	const script = document.createElement('script');

	// Set up promise
	const promise = new Promise((resolve, reject) => {
		script.addEventListener('load', () => resolve(src));
		script.addEventListener('error', () => reject(src));
	});

	// Load script and append to head
	script.src = src;
	container.appendChild(script);

	return promise;
}
