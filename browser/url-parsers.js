/**
 * Check if given link has local hostname
 * @param {Element} link
 * @return {bool}
 */
function isLocalHostname(link) {
	return link.hostname ? location.hostname === link.hostname : true;
}

/**
 * Detect if given URL matches a protocol scheme that should be handled by the client
 * @param {String} url
 * @return {bool}
 */
export function isNativeProtocol(url) {
	const testUrl = test => url.indexOf(test) === 0;
	if (testUrl('mailto:') || testUrl('tel:') || testUrl('sms:')) return true;
	return false;
}

/**
 * Compare given URLs domain name to local domain to determine if URL is external
 * @param {String} url
 * @return {bool}
 */
export function isExternalUrl(url) {
	let link = document.createElement('a');
	link.href = url;
	return !isLocalHostname(link);
}

/**
 * Get local path from fully qualified URL if on the same domain name.
 * @param {String} url
 * @param {boolean} force
 * @return {String}
 */
export function getLocalPath(url, force = false) {
	let link = document.createElement('a');
	link.href = url;
	if (force || (isLocalHostname(link) && !isNativeProtocol(url))) {
		let path = link.pathname + link.search + link.hash;
		path = path.substr(0, 1) === '/' ? path : '/' + path;
		return path;
	}
	return url;
}

/**
 * Add given name and value to given url
 * @param {String} url
 * @param {String} name
 * @param {String} value
 * @return {String} url
 */
export function addQueryParam(url, name, value) {
	if (name && value) {
		let seperator = url.indexOf('?') === -1 ? '?' : '&';
		url += `${seperator}${name}=${value}`;
	}
	return url;
}

/**
 * Get value of given query parameter from current URL.
 * @param {string} name
 * @return {string} value
 */
export const getQueryParam = name => {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	var results = regex.exec(window.location.search);
	return results === null
		? ''
		: decodeURIComponent(results[1].replace(/\+/g, ' '));
};
