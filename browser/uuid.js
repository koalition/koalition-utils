import Modernizr from 'helpers/modernizr';

/**
 * Almost unique ID creator (RFC4122 version 4 compliant solution)
 * @see https://stackoverflow.com/a/2117523/799327
 * @return {String} id
 */
let uuidv4;

if (Modernizr.getrandomvalues) {
	//https://developer.mozilla.org/en-US/docs/Web/API/Window/crypto
	const cryptoObj = global.crypto || global.msCrypto; // for IE 11

	uuidv4 = () =>
		([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
			(
				c ^
				(cryptoObj.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
			).toString(16)
		);
} else {
	uuidv4 = () =>
		'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			let r = (Math.random() * 16) | 0,
				v = c == 'x' ? r : (r & 0x3) | 0x8;
			return v.toString(16);
		});
}

export default uuidv4;
