/**
 * Get window scroll position
 *
 * @return {Object}
 *   @property {Number} x
 *   @property {Number} y
 */
export default function getScroll() {
	let { scrollX, scrollY } = window || {};

	if (scrollX === undefined || scrollY === undefined) {
		scrollX =
			(document.documentElement && document.documentElement.scrollLeft) ||
			(document.body && document.body.scrollLeft) ||
			0;
		scrollY =
			(document.documentElement && document.documentElement.scrollTop) ||
			(document.body && document.body.scrollTop) ||
			0;
	}

	return {
		x: scrollX,
		y: scrollY,
	};
}
