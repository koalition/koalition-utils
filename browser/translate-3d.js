import Modernizr from 'helpers/modernizr';

/**
 * Standard translate function
 * @param {Object} coordinates
 *   @property {Number} x
 *   @property {Number} y
 *   @property {Number} z
 * @param {String} unit
 * @return {String}
 */
export default function translate3d({ x = 0, y = 0, z = 0 }, unit = 'px') {
	x = typeof x === 'number' ? `${x}${unit}` : x;
	y = typeof y === 'number' ? `${y}${unit}` : y;
	z = typeof z === 'number' ? `${z}${unit}` : z;
	return Modernizr.csstransforms3d
		? `translate3d(${x}, ${y}, ${z})`
		: `translateX(${x}) translateY(${y}) translateZ(${z})`;
}
