/**
 * Get coordinates from pointer event
 *
 * @param {Object} event
 * @return {Object}
 *   @property {Number} x
 *   @property {Number} y
 */
const getCoordinatesFromEvent = event =>
	event.touches && event.touches[0]
		? {
				x: event.touches[0].clientX,
				y: event.touches[0].clientY,
		  }
		: {
				x: event.clientX,
				y: event.clientY,
		  };

export default getCoordinatesFromEvent;
