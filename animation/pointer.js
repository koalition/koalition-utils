import Coordinate from './coordinate';

/**
 * @class
 * Animates 2D coordinate based on last seen pointer position.
 */
export default class Pointer extends Coordinate {
	/**
	 * @constructor
	 * @param settings {Object}
	 *   @property coordinates {Array}
	 *   @property element {element}
	 *   @property springSettings {Object} - The same settings as the Spring class.
	 *     @property {Number} acceleration - stiffness / acceleration
	 *     @property {Number} decimals - Number of decimals to determine target accuracy
	 *     @property {Number} friction
	 */
	constructor({ coordinate, element = document, springSettings }) {
		super({ coordinate, springSettings });

		this._element = element;
		element.addEventListener('touchmove', this._onMove);
		element.addEventListener('mousemove', this._onMove);
	}

	/**
	 * Event listener
	 * @private
	 * @param event {Object}
	 */
	_onMove = event =>
		(this.target =
			event.touches && event.touches[0]
				? [event.touches[0].clientX, event.touches[0].clientY]
				: [event.clientX, event.clientY]);

	/**
	 * Destroy
	 * @public
	 */
	destroy() {
		this._element.removeEventListener('touchmove', this.onMove);
		this._element.removeEventListener('mousemove', this.onMove);
	}
}
