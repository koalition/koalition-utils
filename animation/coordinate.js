import Spring from './spring';

const DEFAULT_VALUES = [0, 0];

/**
 * @class
 * Animated set of N values, usually used for 2D or 3D coordinates.
 */
export default class Coordinate {
	/**
	 * @constructor
	 * @param settings {Object}
	 *   @property values {Array}
	 *   @property springSettings {Object} - The same settings as the Spring class.
	 *     @property {Number} acceleration - stiffness / acceleration
	 *     @property {Number} decimals - Number of decimals to determine target accuracy
	 *     @property {Number} friction
	 */
	constructor({ values = DEFAULT_VALUES, springSettings }) {
		this._springs = values.map(
			position =>
				new Spring(springSettings, { current: position, target: position })
		);
	}

	/**
	 * Update position
	 */
	update() {
		this._springs.forEach(spring => spring.update());
	}

	/**
	 * Get current values
	 */
	get current() {
		return this._springs.map(spring => spring.current);
	}

	/**
	 * Get target values
	 */
	get target() {
		return this._springs.map(spring => spring.target);
	}

	/**
	 * Set values
	 * @param values {Array}
	 */
	set target(values = []) {
		this._springs.forEach((spring, index) => {
			let coordinate = values[index];
			if (typeof coordinate === 'number') spring.target = coordinate;
		});
		return this.values;
	}
}
