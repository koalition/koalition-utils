const DEFAULT_STATE = {
		current: 0,
		target: 0,
		velocity: 0,
	},
	DEFAULT_SETTINGS = {
		acceleration: 0.2,
		decimals: 2,
		friction: 0.15,
	};

/**
 * @class
 * Animates towards a given target using basic animation / spring logic (acceleration, friction and velocity)
 */
export default class Spring {
	// -------------------- Initialization --------------------

	/**
	 * @constructor
	 * Set base settings for spring.
	 * @param {Object} settings
	 *   @property {Number} acceleration - stiffness / acceleration
	 *   @property {Number} decimals - Number of decimals to determine target accuracy
	 *   @property {Number} friction
	 * @param {Object} state
	 *   @property {Number} current - current position
	 *   @property {Number} target - target position
	 *   @property {Number} velocity - velocity
	 */
	constructor(settings = {}, state = {}) {
		this._settings = Object.assign({}, DEFAULT_SETTINGS, settings);
		this._state = Object.assign({}, DEFAULT_STATE, state);
	}

	// -------------------- Setters / getters --------------------

	/**
	 * Get current settings
	 * @public
	 */
	get settings() {
		return Object.assign({}, this._settings);
	}

	/**
	 * Update settings
	 * @public
	 * @param {Object} settings
	 *   @property {Number} acceleration - stiffness / acceleration
	 *   @property {Number} friction
	 *   @property {Number} decimals - Number of decimals to determine target accuracy
	 */
	set settings(newSettings) {
		this._settings = Object.assign({}, this._settings, newSettings);
		return this.settings;
	}

	/**
	 * Get current state
	 * @public
	 */
	get state() {
		return {
			current: this.current,
			target: this.target,
			velocity: this.velocity,
		};
	}

	/**
	 * Hard update internal state
	 * @public
	 * @param {Object} state
	 *   @property {Number} current - current position
	 *   @property {Number} target - target position
	 *   @property {Number} velocity - velocity
	 */
	set state({ current, target, velocity }) {
		const { decimals } = this._settings;
		const newState = {};
		if (current)
			newState.current = Math.round(current * Math.pow(10, decimals));
		if (target) newState.target = Math.round(target * Math.pow(10, decimals));
		if (velocity)
			newState.velocity = Math.round(velocity * Math.pow(10, decimals));
		this._state = Object.assign({}, DEFAULT_STATE, newState);
		return this.state;
	}

	/**
	 * Get target
	 * @public
	 */
	get target() {
		return this._state.target / Math.pow(10, this._settings.decimals);
	}

	/**
	 * Set new target
	 * @public
	 * @param {Number} target
	 */
	set target(target = 0) {
		const roundedTarget = Math.round(
			target * Math.pow(10, this._settings.decimals)
		);
		if (roundedTarget !== this._state.target)
			this._state.target = roundedTarget;
		return this.target;
	}

	/**
	 * Get current value
	 * @public
	 */
	get current() {
		return this._state.current / Math.pow(10, this._settings.decimals);
	}

	// -------------------- Methods --------------------

	/**
	 * Update state and let listeners know
	 * @public
	 */
	update() {
		const { acceleration, friction } = this._settings;
		let { current, target, velocity } = this._state;

		// Calculate velocity
		let distance = target - current;
		velocity *= friction;
		velocity += Math.abs(distance) * acceleration;

		// Update current with velocity
		// If our velocity is dead, we've reached our target
		current =
			Math.round(velocity) === 0
				? target
				: Math.round(distance > 0 ? current + velocity : current - velocity);

		// Update internal state
		this._state = {
			current,
			target,
			velocity,
		};

		return this.state;
	}
}
