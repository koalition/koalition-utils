import raf from 'raf';

/**
 * @class
 * Animation wrapper.
 */
export default class Animator {
	/**
	 * @constructor
	 */
	constructor() {
		this._props = {};
	}

	/**
	 * Starts animation
	 * @public
	 *
	 * @param {Function} tick - Animation function to run each cycle. The function will receive two arguments: The current frame index and the time in miliseconds elapsed since the last frame.
	 */
	start(tick) {
		this.stop();

		this.isAnimating = true;
		this._props.frame = 0;
		this._props.lastTime = undefined;

		if (typeof tick === 'function') {
			this._props.tick = tick;
		}

		this._tick();
	}

	/**
	 * Stop animation
	 * @public
	 */
	stop() {
		this.isAnimating = false;
		raf.cancel(this._props.request);
	}

	/**
	 * Run animation
	 * @private
	 */
	_tick() {
		const { lastTime, tick, frame } = this._props;

		// Request next frame (needs to be top of function, so any subsequent calls to `.stop() will work)
		this._props.request = raf(() => this._tick());

		// Update time
		let timeDelta = 0,
			currentTime = Date.now();
		if (lastTime) timeDelta = currentTime - lastTime;
		this._props.lastTime = currentTime;

		// Run provided function
		if (tick) tick(frame, timeDelta);
		this._props.frame++;
	}

	/**
	 * Get current frame
	 */
	get frame() {
		return this._props.frame;
	}
}
