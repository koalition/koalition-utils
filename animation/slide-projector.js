import log from '../log';

const DEFAULT_SETTINGS = {
	animationDuration: 0,
	delay: 30000,
	duration: 6000,
	length: null,
	onChange: null,
};

/**
 * @class
 * High-level logic for slideshows, selecting and index out of N length of slides.
 * Can also be set to automatically progress slide index based on a duration of time.
 */
export default class SlideProjector {
	/**
	 * @constructor
	 * @param {Object} settings
	 *   @property {Number} animationDuration - The time the UI takes to update from one slide to another
	 *   @property {Number} delay - The amount of time the projector should delay automatic progression after a new slide has been selected.
	 *   @property {Number} duration - The amount of time the projector should show each slide
	 *   @property {Number} length - The amount of slides
	 *   @property {Function} onChange - Callback to call when the active slide updates.
	 */
	constructor(settings) {
		this._settings = Object.assign({}, DEFAULT_SETTINGS, settings);
		this._vars = {
			activeIndex: null,
			animation: null,
			delay: null,
			isAnimating: false,
			loop: null,
		};
	}

	// -------------------- Public methods --------------------

	/**
	 * Go to given slide index
	 * @public
	 *
	 * @param index {Number}
	 */
	goTo(index) {
		if (this._vars.isAnimating) return;
		this._setAnimationTimer();
		this._setActiveIndex(index);
	}

	/**
	 * Go to previous slide index
	 * @public
	 */

	goToNext() {
		this.goTo(this._vars.activeIndex + 1);
	}

	/**
	 * Go to next slide index
	 * @public
	 */
	goToPrev() {
		this.goTo(this._vars.activeIndex - 1);
	}

	/**
	 * Go to given slide index and delay automatic slide progression
	 * @public
	 *
	 * @param index {Interger}
	 */
	goToAndDelay(index) {
		if (this._vars.isAnimating) return;
		this._setAnimationTimer();
		this._delayLoop();
		this._setActiveIndex(index);
	}

	/**
	 * Go to next slide index and delay automatic slide progression
	 * @public
	 */
	goToNextAndDelay() {
		this.goToAndDelay(this._vars.activeIndex + 1);
	}

	/**
	 * Go to previous slide index and delay automatic slide progression
	 * @public
	 */
	goToPrevAndDelay() {
		this.goToAndDelay(this._vars.activeIndex - 1);
	}

	/**
	 * Start automatic slide progression
	 * @public
	 */
	startLoop() {
		const { duration, length } = this._settings;

		if (length <= 1)
			return log.debug(
				'SlideProjector.startLoop() - Loop makes no sense with fewer than 2 items.',
				length
			);
		if (!duration)
			return log.debug(
				'SlideProjector.startLoop() - Invalid duration provided to loop.',
				duration
			);

		this.stopLoop();
		this._vars.loop = setTimeout(() => {
			this.goToNext();
			this.startLoop();
		}, duration);
	}

	/**
	 * Stop automatic slide progression
	 * @public
	 */
	stopLoop() {
		clearTimeout(this._vars.loop);
	}

	/**
	 * Update slide projector settings
	 * @public
	 *
	 * @param settings {Object}
	 *   @property {Number} animationDuration
	 *   @property {Number} delay
	 *   @property {Number} duration
	 *   @property {Number} length
	 *   @property {Function} onChange
	 * @param doUpdate {Boolean} - Update internal state based on new settings, particularly length.
	 * @return settings {Object}
	 */
	updateSettings(settings, doUpdate = true) {
		this._settings = Object.assign({}, this._settings, settings);

		if (!doUpdate) return;
		// Make sure index is within potential new length
		const { activeIndex: oldIndex } = this._vars;
		const newIndex = this._sanitizeIndex(oldIndex);
		if (newIndex !== oldIndex) {
			this._setActiveIndex(newIndex);
		}

		return Object.assign({}, this._settings);
	}

	/**
	 * Cleanup internal timers
	 * @public
	 */
	destroy() {
		this.stopLoop();
		clearTimeout(this._vars.delay);
	}

	// -------------------- Private methods --------------------

	/**
	 * Set active index
	 * @private
	 *
	 * @param index {Interger}
	 */
	_setActiveIndex(index = this._vars.activeIndex) {
		const { length } = this._settings;
		if (!length)
			return log.debug(
				'SlideProjector._setActiveIndex() - Invalid length provided.',
				length
			);

		this._vars.activeIndex = this._sanitizeIndex(index);
		this._notify();
	}

	/**
	 * Set animation timer (animation referring to time between slides)
	 * @private
	 *
	 * @param index {Interger}
	 */
	_setAnimationTimer() {
		const { animationDuration } = this._settings;
		if (animationDuration) {
			clearTimeout(this._vars.animation);
			this._vars.isAnimating = true;
			this._vars.animation = setTimeout(
				() => (this._vars.isAnimating = false),
				animationDuration
			);
		}
	}

	/**
	 * Map given index to be within bounds of internal length
	 * @private
	 *
	 * @param index {Interger}
	 * @return index {Interger}
	 */
	_sanitizeIndex(index) {
		const { length } = this._settings;
		index = index % length;
		index = index < 0 ? length + index : index;
		return index;
	}

	/**
	 * Delay automatic progression of slides
	 * @private
	 */
	_delayLoop() {
		const { delay, duration } = this._settings;

		if (!duration)
			return log.debug(
				'SlideProjector.delayLoop() - Invalid duration provided to loop.',
				duration
			);

		this.stopLoop();
		clearTimeout(this._vars.delay);
		this._vars.delay = setTimeout(this.startLoop, delay - duration);
	}

	/**
	 * Update the callback with the current active slide index.
	 * @private
	 */
	_notify() {
		const onChange =
			this._settings.onChange ||
			(() =>
				log.debug(
					'SlideProjector._notify() - No onChange method set.',
					arguments
				));
		onChange(this._vars.activeIndex);
	}
}
