import raf from 'raf';
import Spring from './spring';

/**
 * @class
 * Animates towards a given number using simple Spring logic.
 * Each step updates the animation with requestAnimationFrame
 * until the target has been reached. If the target it updated,
 * animation will restart.
 */
export default class AnimatedSpring extends Spring {
	// -------------------- Initialization --------------------

	/**
	 * Takes the same arguments as the Spring class.
	 * @constructor
	 *
	 * @param {Object} settings
	 *   @property {Number} acceleration - stiffness / acceleration
	 *   @property {Number} decimals - Number of decimals to determine target accuracy
	 *   @property {Number} friction
	 * @param {Object} state
	 *   @property {Number} current - current position
	 *   @property {Number} target - target position
	 *   @property {Number} velocity - velocity
	 */
	constructor() {
		super(...arguments);

		this._listeners = [];
		this._isAnimating = null;
	}

	// -------------------- Listeners --------------------

	/**
	 * Notify consumer listeners
	 * @private
	 *
	 * @param {Object} state
	 */
	_notify(state) {
		const { _listeners } = this;
		for (let i = 0; i < _listeners.length; i++) {
			_listeners[i](state);
		}
	}

	/**
	 * Hook for consumer to register listener
	 * @public
	 *
	 * @param {Function} listener
	 */
	on(listener) {
		return this._listeners.push(listener);
	}

	/**
	 * Hook for consumer to deregister listener
	 * @public
	 *
	 * @param {Number} index
	 */
	off(index) {
		if (index) this._listeners.splice(index, 1);
		else this._listeners = [];

		if (this._listeners.length <= 0) this._destroyListeners();
	}

	// -------------------- Animation --------------------

	/**
	 * Update state and let listeners know
	 * @private
	 */
	_step() {
		const { current, target } = this.update();

		// Animation loop / iteration
		if (target !== current) this._raf();
		else this.stop();

		// Call callback if available
		this._notify(this.state);
	}

	/**
	 * Request animation frame
	 * @private
	 */
	_raf() {
		this._isAnimating = true;
		this._animationFrame = raf(this._step.bind(this));
	}

	/**
	 * Stop animation
	 * @public
	 */
	start() {
		if (!this._isAnimating) this._raf();
	}

	/**
	 * Stop animation
	 * @public
	 */
	stop() {
		if (!this._isAnimating) return;
		clearTimeout(this._timeoutHandle);
		raf.cancel(this._animationFrame);
		this._isAnimating = false;
	}

	// -------------------- Cleanup --------------------

	/**
	 * Destroy
	 * @public
	 */
	destroy() {
		this.stop();
		this.off();
	}
}
