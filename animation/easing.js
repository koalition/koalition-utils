/*
 * @overview
 * Easing Functions - inspired from http://gizma.com/easing/
 * Only considering the `t` value for the range [0, 1] => [0, 1]
 * @see https://gist.github.com/gre/1650294
 */

/**
 * No easing, no acceleration
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const linear = t => t;

/**
 * Accelerating from zero velocity
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeInQuad = t => t * t;

/**
 * Decelerating to zero velocity
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeOutQuad = t => t * (2 - t);

/**
 * Acceleration until halfway, then deceleration
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeInOutQuad = t => (t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t);

/**
 * Accelerating from zero velocity
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeInCubic = t => t * t * t;

/**
 * Decelerating to zero velocity
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeOutCubic = t => --t * t * t + 1;

/**
 * Acceleration until halfway, then deceleration
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeInOutCubic = t =>
	t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;

/**
 * Accelerating from zero velocity
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeInQuart = t => t * t * t * t;

/**
 * Decelerating to zero velocity
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeOutQuart = t => 1 - --t * t * t * t;

/**
 * Acceleration until halfway, then deceleration
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeInOutQuart = t =>
	t < 0.5 ? 8 * t * t * t * t : 1 - 8 * --t * t * t * t;

/**
 * Accelerating from zero velocity
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeInQuint = t => t * t * t * t * t;

/**
 * Decelerating to zero velocity
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeOutQuint = t => 1 + --t * t * t * t * t;

/**
 * Acceleration until halfway, then deceleration
 *
 * @param t {Number} - Number between 0 and 1
 * @return t {Number} - Number between 0 and 1
 */
export const easeInOutQuint = t =>
	t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * --t * t * t * t * t;
