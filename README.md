# koalition-utils

A repo of utility libraries used in Koalition projects.

Intended for use as a submodule. See: https://git-scm.com/book/en/v2/Git-Tools-Submodules

When cloning a repo with submodule use:
`git clone --recurse-submodules <your-project>`
