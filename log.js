import Captain from 'captainslog';

/**
 * Create common logger utility
 */
const cptn = new Captain('koalition-utils');
cptn.toggleDebug(global.site && global.site.isDebug);

export default cptn;
