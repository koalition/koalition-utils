import log from '../log';

/**
 * Default payload transformer just hands back the payload with no transform
 * @param {Object} state - Ignore state object
 * @param {Object} payload - Payload
 * @return {Object} - New state to update
 */
const defaultPayloadTransform = (state, payload) => ({ ...state, ...payload });

/**
 * Default action handler
 * @return {Object} An empty object
 */
const defaultActionHandler = () => ({});

/**
 * A class that stores redux actions and creates handlers for them.
 */
export default class ActionHandler {
	/**
	 * @param {Object} defaultState The default redux state for this reducer
	 */
	constructor(defaultState = null) {
		this._payloadTransforms = {};
		this._reducer = (state = defaultState, { type, payload }) => {
			const transform = this._payloadTransforms[type];
			return transform === undefined ? state : { ...transform(state, payload) };
		};
	}

	/**
	 * Describes and action to be handled in the reducer
	 * @param {String} type A string descriptor for the action
	 * @param {Function} handler Optional function that returns the action payload
	 * @param {Function} payloadTransform Optional function that transforms the payload based upon state
	 * @return {Object}
	 *   @property type
	 *   @property payload
	 */
	createAction = (
		type,
		handler = defaultActionHandler,
		payloadTransform = defaultPayloadTransform
	) => {
		if (!type) {
			log.error(
				`Paramater "type" required in koalition-utils/redux/action-handler! It was ${type}`
			);
			return;
		}
		if (this._payloadTransforms[type]) {
			log.error(
				`Error adding action in koalition-utils/redux/action-handler! Action: ${type} already exists!`
			);
			return;
		}

		this._payloadTransforms[type] = payloadTransform;

		const action = (...args) => ({ type, payload: handler(...args) });

		action.TYPE = type;

		return action;
	};

	/**
	 * Returns the redux reducer
	 * @return {Function}
	 */
	getReducer = () => this._reducer;
}
